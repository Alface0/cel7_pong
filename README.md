# Cel7 Pong

Simple pong implentation to test the [cel7](https://rxi.itch.io/cel7) framework

## Run

```sh
./cel7 pong.fe
```

## Generate binaries

Linux and Windows
```sh
cat cel7 pong.fe > pong
cat cel7.exe pong.fe > pong.exe
```

## Result

Funny and simple pong which results in a linux binary with `24kb` and a windows binary with `34kb`

![Demo](pong_cel7.gif)
